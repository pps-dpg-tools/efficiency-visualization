import sys
import os
# from os import FileNotFoundError

def getOMSCredentials():
    usr = os.getenv('OMS_USER')
    pwd = os.getenv('OMS_PWD')
    if usr is not None and pwd is not None:
        return usr, pwd
    else:
        return None, None  # or return (None, None)

if __name__ == '__main__':
    usr, pwd = getOMSCredentials()
    if usr is not None and pwd is not None:
        print(f"OMS User: {usr}, OMS Password: {pwd}")
    else:
        print("OMS credentials not found.")
    

# def getDBODCredentials():
#     """Get alarm DB credentials from environment. If not found, use utils/alarm_db_credentials.txt

#     Returns:
#         str,str: user,password
#     """
#     usr = os.getenv('DBOD_USER')
#     pwd = os.getenv('DBOD_PWD')
#     if usr is not None and pwd is not None:
#         return usr, pwd
    
# if __name__ == '__main__':
#     usr,pwd = getOMSCredentials()
#     print('Your OMS user is:',usr)
#     print('Your OMS password is:',pwd)
    
    
    # usr,pwd = getDBODCredentials()
    # print('Your Alarm DB user is:',usr)
    # print('Your Alarm DB password is:',pwd)

    # usr,pwd = getCERNSSOCredentials()
    # print('Your CERN SSO user is:',usr)
    # print('Your CERN SSO password is:',pwd)

