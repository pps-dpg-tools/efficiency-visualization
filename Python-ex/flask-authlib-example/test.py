import influxdb


client = influxdb.InfluxDBClient(host='ecal-automation-relay.web.cern.ch', port=80, username='ppsgit', password='dpg4PPS', ssl=False, database='pps_test_v1')
points = client.query('SHOW TAG VALUES ON "pps_test_v1" FROM "run" WITH KEY = "campaign"')
campaigns_names = [row['value'] for row in points.get_points()]
print("compaigns/RUN: ", campaigns_names)
campaigns_names_ = []

for comp in campaigns_names:
    # h = 'test_tracking'
    task1 = "re-tracking-efficiency-analysis-harvester"
    task2 = "tracking-efficiency-analysis-harvester"
    task3 = "tracking-efficiency-reference-harvester"
    task4 = "re-tracking-efficiency-reference-harvester"

    # query = f'SELECT * FROM "run" WHERE "campaign"=\'{comp}\' AND ("{task1}"=\'done\' OR "{task2}"=\'done\' OR "{task3}"=\'done\' OR "{task4}"=\'done\') AND "global"=\'processing\''
    query = f'SHOW TAG VALUES ON "pps_test_v1" FROM "run" WITH KEY = "run_number" WHERE "campaign"=\'{comp}\' AND ("{task1}"=\'done\' OR "{task2}"=\'done\' OR "{task3}"=\'done\' OR "{task4}"=\'done\') AND "global"=\'processing\''
    x = client.query(query)
    runs = [row['value'] for row in x.get_points()]
    print("RUN/run: ", runs)
    # print(comp)
    # print(runs)
    jobruns = []
    for run in runs:
        # points = client.query(f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" WHERE "run_number"=\'{run}\'')
        # points = client.query(f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output"')
        # query = f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" WHERE campaign=%s'
        # points = client.query(query, bind_params={'comp': comp})

        # points = client.query(f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" WHERE campaign=\'{comp}\' ')
        points = client.query(f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" ')
        print(comp)
        # points = client.query('SELECT * FROM "job" WHERE campaign=\'{comp}\' ')
        # points = client.query(f'SELECT * FROM "job" WHERE (campaign=\'{comp}\' AND run_number=\'{run}\') ')

        # outputs = [row['value'] for row in points.get_points() if row['value'] is not None]
        outputs = [row for row in points.get_points()]
        # outputs = [row['value'] for row in points.get_points()]
        # outputs = points.raw['series'][0]['values']
        print("outputs/job: ", (outputs))
        jobruns.append(run)
        # print(outputs)

# comp = 'test_tracking'
# print(comp)
# points = client.query(f'SELECT "output" FROM "job" WHERE campaign=\'{comp}\' AND "run_number"=\'{run}\' ORDER BY time DESC LIMIT 1 ')
points = client.query(f'SELECT "done","task" FROM "job" WHERE campaign=\'{comp}\' AND "run_number"=\'{run}\' AND "done" = 1  AND "task" =\'re-tracking-efficiency-reference-harvester\' ')

# points = client.query(f'SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" WHERE campaign=\'{comp}\' ')
# SELECT "output" FROM "job" WHERE "campaign" = 'zb_with_alignment_20231103' AND "run_number" = '368454'

# points = client.query(f'SHOW FIELD KEYS FROM "job"')
# points = client.query('SHOW TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output" WHERE "done"=1 ')
# points = client.query('SELECT TAG VALUES ON "pps_test_v1" FROM "job" WITH KEY = "output"')

outputs = [row for row in points.get_points()]
# print("o ", outputs[len(outputs)-1])
print("o ", outputs)

