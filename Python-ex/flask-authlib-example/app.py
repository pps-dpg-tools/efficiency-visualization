from flask import Flask, url_for, session, request
from flask import render_template, redirect, send_file, send_from_directory
from authlib.integrations.flask_client import OAuth
from flask import Response
import ROOT as r
import uproot
import matplotlib.pyplot as plt
import numpy as np
import jinja2
# import json
import os
import influxdb

app = Flask(__name__)


# Please change this secret key to some long random string
app.secret_key = '!secret'

CLIENT_ID = os.getenv('CLIENT_ID')
CLIENT_SECRET = os.getenv('CLIENT_SECRET')

CERN_SSO = 'https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration'

oauth = OAuth(app)
oauth.register(
    name='cern',
    server_metadata_url = CERN_SSO,
    client_id = CLIENT_ID,
    client_secret = CLIENT_SECRET,
    client_kwargs = {
        'scope': 'openid email profile'
    }
)

# jinja2.filters.FILTERS['toprettyjson'] = lambda val: json.dumps(val, indent=2)

@app.route('/')
def select():
    client = influxdb.InfluxDBClient(host='ecal-automation-relay.web.cern.ch', port=80, username=CLIENT_ID, password=CLIENT_SECRET, ssl=False, database='pps_test_v1')
    points = client.query('SHOW TAG VALUES ON "pps_test_v1" FROM "run" WITH KEY = "campaign"')
    campaigns_names = [row['value'] for row in points.get_points()]
    campaigns_names_ = []
    RunTasks = ["re-tracking-efficiency-analysis-harvester", "tracking-efficiency-analysis-harvester", "tracking-efficiency-reference-harvester", "re-tracking-efficiency-reference-harvester"]
    xy = 0
    for comp in campaigns_names:
        # query = f'SELECT * FROM "run" WHERE "campaign"=\'{comp}\' AND ("{task1}"=\'done\' OR "{task2}"=\'done\' OR "{task3}"=\'done\' OR "{task4}"=\'done\') AND "global"=\'processing\''
        query = f'SHOW TAG VALUES ON "pps_test_v1" FROM "run" WITH KEY = "run_number" WHERE "campaign"=\'{comp}\' AND ("{RunTasks[0]}"=\'done\' OR "{RunTasks[1]}"=\'done\' OR "{RunTasks[2]}"=\'done\' OR "{RunTasks[3]}"=\'done\') AND "global"=\'processing\''

        x = client.query(query)
        runs = [row['value'] for row in x.get_points()]
        job_runs = []
        job_tasks = []
        for run in runs:
            # print("run: ", run)
            job_sub_task = []
            for task in RunTasks:
                # points = client.query(f'SELECT "output", "task" FROM "job" WHERE campaign=\'{comp}\' AND "run_number"=\'{run}\' AND "task"=\'{task}\' ORDER BY time DESC LIMIT 1 ')
                points = client.query(f'SELECT "output", "task" FROM "job" WHERE campaign=\'{comp}\' AND "run_number"=\'{run}\' AND "task"=\'{task}\' AND "output" != \'\' ORDER BY time DESC LIMIT 1')
                outputs = [row for row in points.get_points()]
                
                if(len(outputs) == 0):
                    continue
                job_sub_task.append(task)

            if(len(job_sub_task)==0):
                continue
            job_runs.append(run)
            job_tasks.append(job_sub_task)

        if(len(job_runs) > 0):        
            campaigns_names_.append([comp, job_runs, job_tasks])

    file_names = campaigns_names_

        # points.raw['series'][0]['values']

    return render_template('option5.html', file_names=campaigns_names_)

@app.route('/process_option', methods=['POST'])
def process_option():

    firstOption = request.form['firstOption']
    secondOption = request.form['secondOption']
    thirdOption = request.form['thirdOption']
    print("Options: ", firstOption, "  ", secondOption, "    ", thirdOption)
    client = influxdb.InfluxDBClient(host='ecal-automation-relay.web.cern.ch', port=80, username=CLIENT_ID, password=CLIENT_SECRET, ssl=False, database='pps_test_v1')
    # points = client.query(f'SELECT "output", "task" FROM "job" WHERE (campaign=\'{firstOption}\' AND "run_number"=\'{secondOption}\' AND "task"=\'{thirdOption}\') ')
    points = client.query(f'SELECT "output", "task" FROM "job" WHERE campaign=\'{firstOption}\' AND "run_number"=\'{secondOption}\' AND "task"=\'{thirdOption}\' AND "output" != \'\' ORDER BY time DESC LIMIT 1')
    outputs = [row for row in points.get_points()]
    print(outputs)
    # outputs = [row['value'] for row in points.get_points()]
    # os.system("rm efficiency-visualization/Python-ex/flask-authlib-example/static/*.png")
    os.system("rm static/*.png")
    if len(outputs)==0:
        error_message = "Error: no corresponding proper output exist for selected compagn, run and task in the job."
        return render_template('home2.html', image_filename=None, error_message=error_message)
    else:
        try:

            file = uproot.open(f'{outputs[len(outputs)-1]["output"]}')
            ffile = r.TFile(f'{outputs[len(outputs)-1]["output"]}', "READ")
        except FileNotFoundError:
            error_message = "Error: The output file does not exist. file: " + f'{outputs[len(outputs)-1]["output"]}'
            return render_template('home2.html', image_filename=None, error_message=error_message)
        keylist = []
        for k in file.keys():
            if "arm" in k and "h2EfficiencyMap" in k :
                keylist.append(k)
        # print(keylist)

        counter = 0
        filenames = []

        for h_itr in range(len(keylist)):
            counter += 1
            h = ffile.Get(keylist[h_itr])
            filenames.append(h.GetName() + "_" + str(counter) + "_" + firstOption + "_" + secondOption + "_" + thirdOption + ".png")

            static_dir = "static"
            if not os.path.exists(static_dir):
                os.mkdir(static_dir)
            
            output_path = "static/" + filenames[counter-1] 
            c = r.TCanvas()
            h.Draw("col z")
            c.Print(output_path)
            del c
            

        file.close()
        ffile.Close()
        return render_template('home2.html', image_filename=filenames, error_message=None)


@app.route('/login')
def login():
    redirect_uri = url_for('auth', _external=True)
    return oauth.cern.authorize_redirect(redirect_uri)


@app.route('/auth')
def auth():
    token = oauth.cern.authorize_access_token()
    session['user'] = token['userinfo']
    return redirect('/')


# @app.route('/logout')
# def logout():
#     session.pop('user', None)
#     return redirect('/')