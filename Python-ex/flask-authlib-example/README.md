# A sample app using Flask and Authlib

This sample app is based on  https://github.com/authlib/demo-oauth-client/tree/master/flask-google-login, and was adapted to CERN environment.


## Install

Install the required dependencies:
    cd Python-ex/flask-authlib-example
    pip install -r requirements.txt

## Install on lxplus

Install the required dependencies:
    scl enable rh-python38
    python3 -m venv .venv
    source .venv/bin/activate
    cd Python-ex/flask-authlib-example
    pip install -r requirements.txt

## Config

* [Create a test application](https://auth.docs.cern.ch/applications/adding-application/) in the Application Portal
* [Register it with CERN SSO as an OIDC application](https://auth.docs.cern.ch/applications/sso-registration/), setting http://localhost:8080/auth as the redirect URI. 
* Put the client (application) ID and secret into environmental variables:
```
export CLIENT_ID=your-client-id
export CLIENT_SECRET=your-client-secret
```    
## Run

Start server with:

    export FLASK_APP=app.py
    flask run -p 8080
    
Then visit http://localhost:8080/
